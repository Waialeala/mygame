using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveData : MonoBehaviour
{
    public StatesData _StateData = new StatesData();

    public StateManager currentStateData;   // обращение к скрипту StateManager
    
    public GameObject StateImageSD;         // обращение к скрипту SpriteChanger
    public SpriteChanger currentSpriteData; //    

    public string path;                     // путь сохранения игры        

    public void SaveDataStart()
    {
        path = Path.Combine(Application.persistentDataPath, "GameData.txt");

        currentStateData = GetComponent<StateManager>();                    // получение компонента из скрипта StateManager     
        currentSpriteData = StateImageSD.GetComponent<SpriteChanger>();     // получение компонента из скрипта SpriteChanger

        LoadFromJson();                                                     // загрузка данных

        currentStateData.GetButtonText();
    }

    public void SaveIntoJson()         // сохранение игровых данных
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(path, FileMode.OpenOrCreate);
        var json = JsonUtility.ToJson(_StateData);
        bf.Serialize(file, json);
        file.Close();

        Debug.Log("Файл успешно сохранен");
    }

    public void LoadFromJson()         // загрузка игровых данных
    {
        if (File.Exists(path) == true)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), _StateData);
            file.Close();

            StopAllCoroutines();
            currentStateData.currentState = _StateData.lastState;
            currentSpriteData.StateImage.sprite = _StateData.lastSprite;

            Debug.Log("Файл успешно загружен");
        }

        else
        {
            Debug.Log("Отсутствует файл сохранения");

            currentStateData.currentState = currentStateData.startingState; 
            currentSpriteData.StateImage.sprite = currentSpriteData.startingSprite;
        }
    }

    public void GetLastData()               // получение последних игровых данных
    {
        _StateData.lastState = currentStateData.currentState;
        _StateData.lastSprite = currentSpriteData.StateImage.sprite;
    }

    public void OnApplicationQuit()
    {
        GetLastData();
        SaveIntoJson();
    }

    public void OnApplicationPause()
    {
        GetLastData();
        SaveIntoJson();
    }

    [Serializable]
    public class StatesData
    {
        public State lastState;
        public Sprite lastSprite;
    }    
}
