using UnityEngine;
using UnityEngine.UI;

public class SpriteChanger : MonoBehaviour
{
    public SpriteRenderer spriteR;
    public Image StateImage;

    public Sprite startingSprite;

    public GameObject B;         // обращение к скрипту StateManager
    public StateManager SM;      //

    public void Start()
    {      
        SM = B.GetComponent<StateManager>();
        spriteR = StateImage.GetComponent<SpriteRenderer>();
    }

    public void ChangeSprite()
    {
        StateImage.sprite = SM.currentState.GetStateImage();        
    }
}
