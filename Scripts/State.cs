using UnityEngine;
using System;

[CreateAssetMenu(menuName = "State")]   // добавление пункта в меню

[Serializable]
public class State : ScriptableObject
{
    [TextArea(25, 14)] public string storyText;  // поле с текстом для заполнения 

    public State[] nextStates;                   // массив следующих State
    public string[] buttonsText;                 // массив текста кнопок текущего State
    public Sprite stateimage;                    // изображение текущего State
    
    public Sprite GetStateImage()
    {
        return stateimage;
    }
    
    public string GetStateStory()
    {
        return storyText;
    }

    public State[] GetNextStates()
    {
        return nextStates;
    }
        
    public string GetButtonText(int index)
    {
        return buttonsText[index];
    }
}
