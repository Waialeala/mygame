using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StateManager : MonoBehaviour
{
    public Text textComponent;               // поле с текстом
    public State startingState;              // начальная сцена (при первом запуске и для сброса прогресса)
    public Text[] buttonText;                
    public GameObject[] ChoiceButtons;

    public State currentState;               

    public SaveData fromSave;                // обращение к скрипту SaveData

    public GameObject StateImageSM;          // обращение к скрипту SpriteChanger
    public SpriteChanger fromSprite;         //

    public void Start()
    {        
        fromSprite = StateImageSM.GetComponent<SpriteChanger>();   // получение компонента изображения
        fromSave = GetComponent<SaveData>();                       // получение компонента сохранения

        fromSave.SaveDataStart();

        textComponent.text = currentState.GetStateStory();
        StartCoroutine("showText", currentState.GetStateStory());
    }
    
    public void GetButtonText()
    {
        for (int i = 0; i != 3; i ++)                       // получение текста кнопок
        {
            buttonText[i].text = currentState.GetButtonText(i);
        }

        for (int i = 0; i != buttonText.Length; i++)        // проверка наличия текста для активации/деактивации кнопок
        {
            if (buttonText[i].text == "") ChoiceButtons[i].SetActive(false);
            else ChoiceButtons[i].SetActive(true);
        }
    }
    

    public void buttonClick(int index)
    {
        StopCoroutine("showText");
        var nextstates = currentState.GetNextStates();      //получение следующих стейтов
        currentState = nextstates[index];
        textComponent.text = currentState.GetStateStory();
        GetButtonText();
        fromSprite.ChangeSprite();    
        StartCoroutine("showText", textComponent.text);
    }

    public void SkipText()             // пропуск анимации текста
    {
        StopAllCoroutines();
        textComponent.text = currentState.GetStateStory();
    }

    public void ResetProgress()        // сброс прогресса
    {
        StopAllCoroutines();
        currentState = startingState;
        textComponent.text = currentState.GetStateStory();
        GetButtonText();
        fromSave._StateData.lastState = startingState;
        fromSprite.StateImage.sprite = fromSprite.startingSprite;
        StartCoroutine("showText", currentState.GetStateStory());
    }

    IEnumerator showText(string text)  // анимация текста + исключение прыгания текста
    {
        int i = 0;
        while (i <= text.Length)
        {
            textComponent.text = text.Substring(0, i) + "<color=#0000>" + text.Substring(i) + "</Color>";
            i++;        
            yield return new WaitForSeconds(0.045f);
        }
    }    
}
